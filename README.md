# Checking Drive Backups

This function is used to get the backups information stored in a Google Drive account using python. 
It's a part of my backup system. Please, check these repositories to get more information:

* My [own Ansible-based configuration system](https://gitlab.com/tangelov/configuration)

* My [own backupninja Ansible-based role](https://gitlab.com/tangelov-roles/backupninja)

This basic function has the following capacities:

* It logins into a Google Drive account via v3 API with a pickle token and it stores it in a Cloud Storage bucket.

* It checks how many folders there are in a Google Drive account. Every folder represents one server to backup.

* It counts them and substract one unit because one backup is optional.

* It checks how many backups have been done today by my "own backup system".

* It compares both numbers and if there are less backups than folders, it sends a new message to a PubSub Topic.


## Authentication

This function must have access to Google Drive (API v3) and Google Cloud (one Cloud Storage bucket and one Cloud Pub/Sub topic)

### Authentication to Google Drive
This function has to login from a Google Cloud organization to a personal _gmail.com_ account into its Google Drive. It's not possible to do it without a _Wide-side permission delegation_ and I don't have found how to do it in a programatic way without any user intervention the first time.

Because of this problem, this function should be executed first at local to create a pickle token and upload it to a bucket prior to execute it in Google Cloud. The function will renew its credentials in next executions and refreshing the token.
### Authentication to Google Cloud

This functions uses the default _GOOGLE_APPLICATION_CREDENTIALS_ of the service account who executes it. The service account should have at least the following IAM roles:

* Storage Legacy Bucket Reader

* Storage Object Admin


## Documentation
* [Python3 Quickstart for Google Drive API v3](https://developers.google.com/drive/api/v3/quickstart/python)

* [Google Cloud Storage documentation for Python 3](https://googleapis.dev/python/storage/latest/client.html)

* [Google Cloud PubSub documentation for Python 3](https://googleapis.dev/python/pubsub/latest/index.html)

* [Testing Google Cloud PubSub functions in Python](https://cloud.google.com/functions/docs/testing/test-background#functions-testing-pubsub-unit-python)