# Generic libraries
import os, sys
import logging
from datetime import date

# Dependencies related to Google Drive interactions
import pickle
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# Dependencies related to Google Cloud 
from google.cloud import pubsub_v1
from google.cloud import storage

# Vars from Google Cloud environment
GCS_BUCKET = os.getenv("GCS_BUCKET")
GCP_PUBSUB_TOPIC = os.getenv("GCP_PUBSUB_TOPIC")


def config_logging():
    """This function will configure the logging system
    of the whole function
    """
    
    # Initial configuration of logging system
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level=logging.INFO,
        stream=sys.stdout
    )


def sending_to_pubsub(string, label):
    """This function will upload any correct message
    to a Cloud PubSub topic
    """
    config_logging()

    # Importing the GCP PubSub Topic from the environment variables
    GCP_PUBSUB_TOPIC = os.getenv("GCP_PUBSUB_TOPIC")

    # Creation of a PubSub publisher in GCP_PUBSUB_TOPIC
    publisher = pubsub_v1.PublisherClient()
    topic_name = GCP_PUBSUB_TOPIC
    logging.info("Creating a Cloud PubSub client to send a message to a topic.")

    # Publishing message with date label
    message = publisher.publish(topic_name, b'%s' % str.encode(string), date=label)
    message.result()
    logging.info("Message sent to %s PubSub topic" % topic_name)

    # Adding a return to the function to test it
    return string


def checking_backups(event, context):
    """This function uses Cloud Storage and Google Drive v3 API
    to check the state of one Google Drive account.
    """

    # Importing GCS bucket inside the function
    GCS_BUCKET = os.getenv("GCS_BUCKET")

    # If we modify these scopes, we must delete the file token.pickle
    # from Google Cloud Storage Bucket
    # It's necessary to run this function one time outside of cloud functions
    # to get the pickle token with user intervention.
    SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']

    # Init logging system
    config_logging()
    
    creds = None

    # First the take the picke file from a GCS bucket if exists
    logging.info("Creating a Cloud Storage client to check if Drive token exists.")
    client = storage.Client()
    bucket = client.get_bucket(GCS_BUCKET)
    blob = storage.Blob('token.pickle', bucket)
    # We need to define a new instance of the same blob to avoid CRC errors during
    # the upload of the files to GCS for next executions.
    blob_tmp = storage.Blob('token.pickle', bucket)

    if blob.exists():
        with open('/tmp/token.pickle', 'wb') as token:
            client.download_blob_to_file(blob, token)
            logging.info("Getting credentials from token.pickle file.")

    # The file token.pickle stores the user's access and refresh tokens.
    # It is created automatically when the authorization flow completes
    # for the first time.
    if os.path.exists('/tmp/token.pickle'):
        with open('/tmp/token.pickle', 'rb') as token:
            creds = pickle.load(token)
            logging.info("Loading credentials from token.pickle.")

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        logging.warning("Credentials not valid of expired.")
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
            logging.info("Drive Credentials renewed successfuly.")

        else:
            logging.error("Cannot renew credentials. Renew them in a local PC")
            creds = flow.run_local_server(port=0)

        # Save the credentials for the next run
        with open('/tmp/token.pickle', 'wb') as token:
            pickle.dump(creds, token)

        # Saving the new pickle file in a GCS bucket for the net run
        with open('/tmp/token.pickle', 'rb') as token:
            blob_tmp.upload_from_file(token)
            logging.info("Reuploading new valid credentials to Google Cloud Storage")

    # Using credentials to create a service against Google Drive
    try:
        service = build('drive', 'v3', credentials=creds)
        logging.info("Creating a sucessfull client against Google Drive API v3.")
    
    except:
        logging.error("Cannot create a connection against Google Drive API v3.")


    # Getting date in my particular format
    today = (date.today()).strftime("%Y.%m.%d")

    # Every server has a dedicated folder but one is not mandatory.
    folders = service.files().list(
           q="mimeType = 'application/vnd.google-apps.folder'",
           spaces='drive',
           fields='nextPageToken, files(name)').execute()
    logging.info("Get a list of folders in Google Drive storage.")

    # We have one inusual backup and two regular backups and one folder
    # per server to backup in Google Drive. The number of folders represents
    # the number of total backups.
    total_folders = (len(folders['files'])) - 1
    logging.info("Total folders %s." % total_folders)

    # We search all files with YYYY.MM.dd date format in Google Drive
    backups = service.files().list(
        q="name contains '%s'" % (today),
        spaces='drive',
        fields='nextPageToken, files(name)').execute()
    logging.info("Calculating number of backups available in Google Drive.")

    # We count the number of backups with a specific date
    total_backups = len(backups['files'])
    logging.info("Total backups: %s." % total_backups)

    # If there is more or equal backups all is fine. We don't send a message
    # if all is fine. I know that it's possible the inusual backups can give
    # me some temporal false positives but it is not dangerous.
    if total_backups >= total_folders:
        message = "Backups have been done correctly. Exiting..."
        logging.info(message)

    # If something is wrong, publish a message in a PubSub topic
    else:
        message = "Something is wrong with the backups system at %s" % today
        sending_to_pubsub(message, today)

    # We return the message, it will help the testing process
    return message
