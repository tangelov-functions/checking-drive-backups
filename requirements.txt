# General dependencies
datetime==5.5

# Dependencies related with Google Drive
google-api-python-client==2.161.0
google-auth-httplib2==0.2.0
google-auth-oauthlib==1.2.1

# Dependencies related with Google Cloud
google-cloud-pubsub==2.28.0
google-cloud-storage==2.19.0
