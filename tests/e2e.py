import os, sys
from unittest import mock

import pytest

# We install all  the dependencies in the folder lib
file_path = os.path.dirname(__file__)
module_path = os.path.join(file_path, "../lib")
sys.path.append(module_path)

# We import the main library from the parent folder
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import main

# Create a mock context
mock_context = mock.Mock()

# Checking e2e functionality in the main checking_backup function
def test_main_function():
    checking = main.checking_backups("backup-checker", mock_context)
    assert "backups" in checking.lower()


# Checking e2e functionality in the message_to_matrix function
def test_pubsubg_function():
    checking = main.sending_to_pubsub("Test E2E para procesar mensajes", "")
    assert checking == "Test E2E para procesar mensajes"
